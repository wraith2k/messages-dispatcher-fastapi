from enum import Enum
from inspect import get_annotations


class ValueExtraInfo:
    def __init__(self, desc: str):
        self.desc = desc


class EnumWithDesc(Enum):
    @classmethod
    def _get_meta(cls, var_name: str) -> tuple:
        annotation: dict | None = get_annotations(cls).get(var_name)
        if not annotation:
            return ()
        return getattr(annotation, '__metadata__', ())

    @classmethod
    def make_desc(cls) -> str:
        descs_str = ''
        for enum_name, enum_val in cls.__members__.items():
            desc_list = [
                meta_val.desc
                for meta_val in cls._get_meta(enum_name)
                if isinstance(meta_val, ValueExtraInfo)]
            descs_str += f'* `{enum_val.value}`{" - " if desc_list else ""}' \
                         f'{" ".join(desc_list)}\n'
        return descs_str
