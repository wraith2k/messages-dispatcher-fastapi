from typing import Any

from dynaconf import Dynaconf  # type: ignore
from pydantic import BaseModel


class WithPydanticSettings(BaseModel):
    def __init__(self, **kwargs: Any) -> None:
        dyn_settings = Dynaconf(
            settings_files=['settings.yaml', '.secrets.yaml'],
            load_dotenv=True,
            environments=True
        )
        dict_from_dynaconf = self.model_validate(
            dyn_settings, from_attributes=True).model_dump(exclude_unset=True)
        dict_for_model = dict_from_dynaconf | kwargs
        super().__init__(**dict_for_model)
