from celery import Celery  # type: ignore

from app.core.config import settings

celery_app = Celery(broker='amqp://guest@127.0.0.1//',
                    include='app.tasks.msg_list_processing')
# Вместо параметра include можно использовать так:
# celery_app.autodiscover_tasks(['app.tasks'],related_name='celery_tasks')

celery_app.conf.update(
    {
        'beat_dburi': str(settings.SQLALCHEMY_DATABASE_URI_SYNC),
        'beat_schema': settings.BEAT_SCHEMA,
        'beat_scheduler': 'sqlalchemy_celery_beat.schedulers:DatabaseScheduler',  # noqa: E501
    }
)
