from typing import Annotated
from pydantic import Field, AnyHttpUrl, PostgresDsn, model_validator
from app.core.base_settings import WithPydanticSettings
from app import __version__


class Settings(WithPydanticSettings):
    VERSION: str = ''
    API_BASE: Annotated[str, Field(pattern=r'^\/.+')] = '/api'
    BACKEND_CORS_ORIGINS: \
        Annotated[list[AnyHttpUrl], Field(min_items=1)] | None = None
    PROJECT_NAME: Annotated[str, Field(min_length=1)] = 'New Project'
    TABLES_PREFIX: Annotated[
        str, Field(min_length=1, max_length=50)] = 'newapp_'
    DB_USER_NAME: Annotated[str, Field(min_length=1)]
    DB_USER_PASSWORD: Annotated[str, Field(min_length=1)]
    DB_NAME: Annotated[str, Field(min_length=1)]
    DB_SERVER: Annotated[str, Field(min_length=1)] = '127.0.0.1'
    DB_SERVER_PORT: Annotated[str, Field(pattern=r'^\d{1,5}$')] = '5432'
    SQLALCHEMY_DATABASE_URI_ASYNC: PostgresDsn | None = None
    SQLALCHEMY_DATABASE_URI_SYNC: PostgresDsn | None = None
    # Интервал минут, при превышении которого рассылка по списку будет
    # запланирована через celery beat. Иначе задача
    # рассылки отправляется напрямую в celery worker
    SCHEDULE_BREAKPOINT_TIMEDELTA: Annotated[int, Field(ge=0, le=60)] = 5
    BEAT_SCHEMA: Annotated[str, Field(min_length=1)] | None = None
    MESSAGE_API_BASE: AnyHttpUrl
    MESSAGE_API_TOKEN: Annotated[str, Field(min_length=1)]

    @model_validator(mode='after')
    def set_version(self) -> 'Settings':
        self.VERSION = __version__
        return self

    @model_validator(mode='after')
    def assemble_db_connections(self) -> 'Settings':
        if not self.SQLALCHEMY_DATABASE_URI_ASYNC:
            self.SQLALCHEMY_DATABASE_URI_ASYNC = PostgresDsn.build(
                scheme="postgresql+asyncpg",
                username=self.DB_USER_NAME,
                password=self.DB_USER_PASSWORD,
                host=self.DB_SERVER,
                path=f'{self.DB_NAME}',
            )

        if not self.SQLALCHEMY_DATABASE_URI_SYNC:
            self.SQLALCHEMY_DATABASE_URI_SYNC = PostgresDsn.build(
                scheme="postgresql+psycopg",
                username=self.DB_USER_NAME,
                password=self.DB_USER_PASSWORD,
                host=self.DB_SERVER,
                path=f'{self.DB_NAME}',
            )
        return self


settings = Settings()  # type: ignore
