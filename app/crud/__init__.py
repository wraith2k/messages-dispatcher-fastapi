from app.crud.base import CRUDBase
from app.crud.crud_message_log import CRUDMessageLog
from app.models.message_log import MessageLog
from app.models.msg_list import MsgList
from app.models.client import Client
from app.schemas import MsgListCreatePut, MsgListPatch, ClientCreatePut, \
    ClientPatch, MessageLogCreatePut

msg_list = CRUDBase[MsgList, MsgListCreatePut, MsgListPatch](MsgList,
                                                             MsgListCreatePut)
client = CRUDBase[Client, ClientCreatePut, ClientPatch](Client,
                                                        ClientCreatePut)
message_log = CRUDMessageLog(MessageLog, MessageLogCreatePut)
