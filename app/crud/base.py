from typing import TypeVar, Generic, Type, Sequence

from fastapi.exceptions import RequestValidationError
from pydantic import BaseModel, ValidationError
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.base_class import Base

# Can be any subtype of Base (SQLAlchemy database model)
ModelType = TypeVar("ModelType", bound=Base)
# Can be any subtype of BaseModel (Pydantic schema model)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
PartialUpdateSchemaType = TypeVar("PartialUpdateSchemaType", bound=BaseModel)


class CRUDBase(Generic[ModelType, CreateSchemaType, PartialUpdateSchemaType]):
    def __init__(self, model: Type[ModelType], schema: Type[CreateSchemaType]):
        """
        CRUD object with default methods to
        Create, Read, Update, Delete (CRUD).

        Default implementation not support relation fields preload
        (you have to avoid relation fields in schema)

        **Parameters**

        * `model`: A SQLAlchemy model class
        * `schema`: A Pydantic model (schema) class
        """
        self.model = model
        self.schema = schema

    async def get(
            self,
            async_session: AsyncSession,
            obj_id: int) -> ModelType | None:
        # stmt = select(self.model).where(self.model.id == obj_id)
        # return await async_session.scalar(stmt)
        return await async_session.get(self.model, obj_id)

    async def get_multi(
            self,
            async_session: AsyncSession,
            skip: int | None = None, limit: int | None = None
    ) -> Sequence[ModelType]:
        stmt = select(self.model).order_by(self.model.id)
        if skip:
            stmt = stmt.offset(skip)
        if limit:
            stmt = stmt.limit(limit)
        return (await async_session.scalars(stmt)).all()

    # async def refresh_obj(
    #         self,
    #         async_session: AsyncSession,
    #         db_obj: ModelType) -> ModelType:
    #     stmt = select(self.model).where(self.model.id == db_obj.id) \
    #         .execution_options(populate_existing=True)
    #
    #     await async_session.execute(stmt)
    #     return db_obj

    async def create(
            self,
            async_session: AsyncSession,
            obj_in: CreateSchemaType) -> ModelType:
        db_obj = self.model(**obj_in.model_dump())
        async_session.add(db_obj)
        await async_session.commit()
        await async_session.refresh(db_obj)
        return db_obj

    async def update(
            self,
            async_session: AsyncSession,
            db_obj: ModelType,
            obj_in: PartialUpdateSchemaType
    ) -> ModelType:
        # Получаем схему из БД
        mdl_in_db = self.schema.model_validate(db_obj, from_attributes=True)
        # Заменяем в схеме из БД поля на заданные поля из запроса
        mdl_to_check = mdl_in_db.model_copy(
            update=obj_in.model_dump(exclude_unset=True))
        # Выполняем все validation check входящих данных с учетом данных из БД
        try:
            self.schema.model_validate(mdl_to_check)
        except ValidationError as e:
            raise RequestValidationError(errors=e.errors())

        # Если проверка прошла, то записываем в БД
        obj_in_data = obj_in.model_dump(exclude_unset=True)
        for k, v in obj_in_data.items():
            if hasattr(db_obj, k):
                setattr(db_obj, k, v)

        async_session.add(db_obj)
        await async_session.commit()
        await async_session.refresh(db_obj)
        return db_obj

    async def remove(
            self,
            async_session: AsyncSession,
            obj_id: int) -> ModelType | None:
        # stmt = select(self.model).where(self.model.id == obj_id) \
        #     .options(load_only(self.model.id))
        stmt = select(self.model).where(self.model.id == obj_id)
        obj = await async_session.scalar(stmt)
        if not obj:
            return obj

        await async_session.delete(obj)
        await async_session.commit()
        return obj
