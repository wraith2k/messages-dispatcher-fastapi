from fastapi import HTTPException
from pydantic import PositiveInt
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud import CRUDBase
from app.models.client import Client
from app.models.message_log import MessageLog
from app.models.msg_list import MsgList
from app.schemas import MessageLogCreatePut, MessageLogPatch


class CRUDMessageLog(CRUDBase[
                         MessageLog,
                         MessageLogCreatePut,
                         MessageLogPatch]):
    async def create(
            self,
            async_session: AsyncSession,
            obj_in: MessageLogCreatePut,
            msg_list_in: PositiveInt | MsgList | None = None,
            client_in: PositiveInt | Client | None = None) -> MessageLog:
        if isinstance(msg_list_in, int):
            msg_list = await async_session.get(MsgList, msg_list_in)
            if not msg_list:
                raise HTTPException(
                    status_code=404,
                    detail=f"MsgList with ID={msg_list_in} not found")
        else:
            msg_list = msg_list_in

        if isinstance(client_in, int):
            client = await async_session.get(Client, client_in)
            if not client:
                raise HTTPException(
                    status_code=404,
                    detail=f"Client with ID={client_in} not found")
        else:
            client = client_in

        db_obj = self.model(
            **obj_in.model_dump(),
            msg_list=msg_list,
            client=client)
        async_session.add(db_obj)
        await async_session.commit()
        await async_session.refresh(db_obj)
        return db_obj
