import re
from typing import Annotated

from sqlalchemy.ext.asyncio import AsyncAttrs
from sqlalchemy.orm import DeclarativeBase, declared_attr, Mapped, \
    mapped_column

from app.core.config import settings

IntPKField = Annotated[int, mapped_column(primary_key=True, index=True)]


class Base(AsyncAttrs, DeclarativeBase):
    id: Mapped[IntPKField]

    @staticmethod
    def generate_table_name(class_name: str) -> str:
        return settings.TABLES_PREFIX + class_name.lower()

    @staticmethod
    def camel_to_snake(name: str) -> str:
        return re.sub(r'(?!^)(?=[A-Z])', '_', name).lower()

    # Generate __tablename__ automatically
    @declared_attr.directive
    @classmethod
    def __tablename__(cls) -> str:
        return cls.generate_table_name(cls.__name__)
