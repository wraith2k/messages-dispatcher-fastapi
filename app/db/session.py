from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker

from app.core.config import settings

engine = create_async_engine(
    str(settings.SQLALCHEMY_DATABASE_URI_ASYNC),
    echo=False)
engine = engine.execution_options(
    schema_translate_map={'celery_schema': settings.BEAT_SCHEMA},
)
SessionLocal = async_sessionmaker(bind=engine, expire_on_commit=False)
