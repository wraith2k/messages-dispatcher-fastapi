# Import all the models, so that Base has them before being
# imported by Alembic
from app.db.base_class import Base  # noqa: F401
from app.models.msg_list import MsgList  # noqa: F401
from app.models.client import Client  # noqa: F401
from app.models.message_log import MessageLog  # noqa: F401
