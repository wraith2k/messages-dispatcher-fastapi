from typing import Sequence
from pydantic import PositiveInt
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload
from app.models.message_log import MessageLog
from app.models.client import Client  # noqa: F401
from app.models.msg_list import MsgList  # noqa: F401


async def detail_report(
        async_session: AsyncSession,
        msg_list_id: PositiveInt) -> Sequence[MessageLog]:
    stmt = select(MessageLog).where(MessageLog.msg_list_id == msg_list_id
                                    ).options(joinedload(MessageLog.client))
    return (await async_session.scalars(stmt)).all()
