from typing import Sequence

from sqlalchemy import select, text, func, values, String
from sqlalchemy.dialects import postgresql
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Bundle
from sqlalchemy.sql import column
from app.models.msg_list import MsgList
from app.models.message_log import MessageLog
from app.schemas.common import MessageStatusEnum


async def overall_report(async_session: AsyncSession) -> Sequence:
    stmt_ext = text('CREATE EXTENSION IF NOT EXISTS tablefunc')
    await async_session.execute(stmt_ext)
    stmt_src = select(
        MsgList.id,
        MsgList.date_start,
        MsgList.date_end,
        MessageLog.status,
        func.count(MessageLog.id).label('count')
    ).join(MsgList, isouter=True).group_by(
        MsgList.id,
        MsgList.date_start,
        MsgList.date_end,
        MessageLog.status
    ).order_by(MsgList.id)

    # Чтобы отобразить все рассылки, а не только те по которым есть сообщения,
    # можно делать так:
    # stmt_src = select(
    #     MsgList.id,
    #     MsgList.date_start,
    #     MsgList.date_end,
    #     MessageLog.status,
    #     func.count(MessageLog.id).label('count')
    # ).join(MessageLog, full=True).group_by(
    #     MsgList.id,
    #     MsgList.date_start,
    #     MsgList.date_end,
    #     MessageLog.status
    # ).order_by(MsgList.id)

    tbl_cat = values(
        column("status", String),
        name="categories",
    ).data([(status.value,) for status in MessageStatusEnum])
    stmt_cat = select(tbl_cat)

    str_src = stmt_src.compile(dialect=postgresql.dialect())
    str_cat = stmt_cat.compile(dialect=postgresql.dialect(),
                               compile_kwargs={"literal_binds": True})
    str_res = ', '.join(
        [
            'id int',
            'date_start timestamp with time zone',
            'date_end timestamp with time zone'] +
        [f'{status.value} int' for status in MessageStatusEnum])
    str_fields = ', '.join(
        ['id', 'date_start', 'date_end'] +
        [f'COALESCE({status.value},0) as {status.value}'
         for status in MessageStatusEnum])
    txt_sql = f'select {str_fields} ' \
              f'from crosstab($${str_src}$$,$${str_cat}$$) as ({str_res})'
    stmt = select(
        column('id'),
        column('date_start'),
        column('date_end'),
        Bundle('statuses',
               *[column(status.value) for status in MessageStatusEnum])
    ).from_statement(text(txt_sql))

    return (await async_session.execute(stmt)).all()
