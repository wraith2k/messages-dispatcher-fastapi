from typing import TYPE_CHECKING, Union

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, relationship, declared_attr, mapped_column

if TYPE_CHECKING:
    from app.models.message_log import MessageLog
    from app.models.msg_list import MsgList
    from app.models.client import Client


class MessageLogManyToOneMixin:
    @declared_attr
    @classmethod
    def message_log_set(cls) -> Mapped[list['MessageLog']]:
        camel_to_snake = getattr(cls, 'camel_to_snake', None)
        if not callable(camel_to_snake):
            raise Exception('Incompatible parent class')
        return relationship(back_populates=camel_to_snake(cls.__name__))


class MsgListOneToManyMixin:
    @declared_attr
    @classmethod
    def msg_list_id(cls) -> Mapped[int | None]:
        generate_table_name = getattr(cls, 'generate_table_name', None)
        if not callable(generate_table_name):
            raise Exception('Incompatible parent class')
        return mapped_column(
            ForeignKey(generate_table_name('MsgList') + '.id'))

    @declared_attr
    @classmethod
    def msg_list(cls) -> Mapped[Union['MsgList', None]]:
        camel_to_snake = getattr(cls, 'camel_to_snake', None)
        if not callable(camel_to_snake):
            raise Exception('Incompatible parent class')
        return relationship(
            back_populates=camel_to_snake(cls.__name__) + '_set')


class ClientOneToManyMixin:
    @declared_attr
    @classmethod
    def client_id(cls) -> Mapped[int | None]:
        generate_table_name = getattr(cls, 'generate_table_name', None)
        if not callable(generate_table_name):
            raise Exception('Incompatible parent class')
        return mapped_column(
            ForeignKey(generate_table_name('Client') + '.id'))

    @declared_attr
    @classmethod
    def client(cls) -> Mapped[Union['Client', None]]:
        camel_to_snake = getattr(cls, 'camel_to_snake', None)
        if not callable(camel_to_snake):
            raise Exception('Incompatible parent class')
        return relationship(
            back_populates=camel_to_snake(cls.__name__) + '_set')
