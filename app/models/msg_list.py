from datetime import datetime, time
from sqlalchemy import String
from sqlalchemy.dialects.postgresql import TIMESTAMP, JSONB
from sqlalchemy.orm import Mapped, mapped_column
from app.db.base_class import Base
from app.models.relationship_mixins import MessageLogManyToOneMixin


class MsgList(Base, MessageLogManyToOneMixin):
    date_start: Mapped[datetime] = mapped_column(TIMESTAMP(timezone=True))
    date_end: Mapped[datetime] = mapped_column(TIMESTAMP(timezone=True))
    message_text: Mapped[str] = mapped_column(String(200))
    filter_props: Mapped[dict[str, set] | None] = mapped_column(JSONB)
    allowed_intervals: Mapped[list[tuple[time, time]] | None] = \
        mapped_column(JSONB)

    # print(CreateTable(User.__table__))
