from sqlalchemy import String
from sqlalchemy.orm import Mapped, mapped_column
from app.db.base_class import Base
from app.models.relationship_mixins import MessageLogManyToOneMixin


class Client(Base, MessageLogManyToOneMixin):
    phone: Mapped[str] = mapped_column(String(11))
    mob_code: Mapped[str] = mapped_column(String(5))
    tag: Mapped[str | None] = mapped_column(String(20))
    time_zone: Mapped[str] = mapped_column(String(50))
