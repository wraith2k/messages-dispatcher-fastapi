from datetime import datetime
from sqlalchemy import String
from sqlalchemy.dialects.postgresql import TIMESTAMP
from sqlalchemy.orm import Mapped, mapped_column
from app.db.base_class import Base
from app.models.relationship_mixins import MsgListOneToManyMixin, \
    ClientOneToManyMixin


class MessageLog(Base, MsgListOneToManyMixin, ClientOneToManyMixin):
    date_send: Mapped[datetime] = mapped_column(TIMESTAMP(timezone=True))
    status: Mapped[str] = mapped_column(String(20))
    response_text: Mapped[str | None] = mapped_column(String(200))

    # print(CreateTable(User.__table__))
