import json
from datetime import datetime, timezone, timedelta
from typing import TypeVar

from celery import signature  # type: ignore
from celery.result import AsyncResult  # type: ignore
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy_celery_beat import ClockedSchedule  # type: ignore
from sqlalchemy_celery_beat.models import ScheduleModel, PeriodicTask  # type: ignore  # noqa: E501

from app.core.config import settings
from app.models.msg_list import MsgList
from app.tasks.msg_list_processing import msg_list_process_task

ModelType = TypeVar("ModelType", bound=ScheduleModel)


class TaskForMsgList:
    def __init__(self, obj: MsgList, async_session: AsyncSession):
        self.obj = obj
        self.async_session = async_session

    async def get_schedule(self, dt: datetime) -> ClockedSchedule:
        stmt = select(ClockedSchedule) \
            .where(ClockedSchedule.clocked_time == dt)
        schedule: ClockedSchedule = await self.async_session.scalar(stmt)

        if not schedule:
            schedule = ClockedSchedule(clocked_time=dt)
            self.async_session.add(schedule)
            await self.async_session.commit()
            await self.async_session.refresh(schedule)

        return schedule

    async def create_task(self) -> PeriodicTask | AsyncResult | None:
        now = datetime.now(timezone.utc)
        dt_start: datetime = self.obj.date_start

        if now > self.obj.date_end:
            return None
        if dt_start - now > timedelta(minutes=settings.SCHEDULE_BREAKPOINT_TIMEDELTA):  # noqa: E501
            # Задача будет запланирована в celery beat, чтобы задачи
            # со стартом в дальнем будущем не висели в памяти worker
            task_name = f"list_{self.obj.id}_scheduled_task"
            schedule = await self.get_schedule(dt_start.replace(microsecond=0))
            return await self.create_task_in_db(schedule, task_name)
        # Т.к. задача должна быть выполнена или уже или в ближайшее
        # время, то направляем эту задачу в celery worker
        eta = None if now > dt_start else dt_start
        return msg_list_process_task.apply_async((self.obj.id,), eta=eta)

    async def create_task_in_db(
            self,
            schedule: ClockedSchedule,
            task_name: str) -> PeriodicTask:
        sig: signature = msg_list_process_task.s(self.obj.id)
        periodic_task = PeriodicTask(
            schedule_model=schedule,
            name=task_name,
            task=sig.task,
            args=json.dumps(sig.args),
            one_off=True
        )
        self.async_session.add(periodic_task)
        await self.async_session.commit()
        await self.async_session.refresh(periodic_task)

        return periodic_task
