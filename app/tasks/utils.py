from datetime import datetime, timezone
from typing import Sequence

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from app.models.msg_list import MsgList
from app import crud  # noqa: F401
from app.tasks.task_for_msg_list import TaskForMsgList


async def msg_list_created(obj: MsgList, async_session: AsyncSession) -> None:
    task = TaskForMsgList(obj, async_session)
    await task.create_task()


async def run_active(
        async_session: AsyncSession,
        skip: int | None = None,
        limit: int | None = None) -> Sequence[MsgList]:
    dt_now = datetime.now(timezone.utc)
    stmt = select(MsgList) \
        .where(MsgList.date_end >= dt_now,
               MsgList.date_start <= dt_now) \
        .order_by(MsgList.id)
    if skip:
        stmt = stmt.offset(skip)
    if limit:
        stmt = stmt.limit(limit)
    msg_lists: Sequence[MsgList] = (await async_session.scalars(stmt)).all()

    for msg_list in msg_lists:
        task = TaskForMsgList(msg_list, async_session)
        await task.create_task()
    return msg_lists
