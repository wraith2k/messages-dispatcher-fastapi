import asyncio
from typing import Iterable

import aiohttp
import random
from datetime import datetime, timezone
from celery import shared_task  # type: ignore
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload
# shared_task will use Celery instance defined in 'app.core.celery_app'
import app.core.celery_app  # noqa: F401

from app.core.config import settings
from app.db.session import SessionLocal, engine
# Import crud to be sure what all related models loaded in registry
from app import crud  # noqa: F401
from app.models.client import Client
from app.models.message_log import MessageLog
from app.models.msg_list import MsgList
from app.schemas import ClientInDB, MsgListInDB
from app.schemas.message_log import MessageStatus, MessageLogCreatePut


async def send_msg(
        msg_lst: MsgList | None,
        client: Client | None,
        http_session: aiohttp.ClientSession,
        async_session: AsyncSession,
        n_retries: int = 5,
        message_log: MessageLog | None = None) -> None:
    async def send_request(url: str) -> tuple[bool, str]:
        try:
            async with http_session.post(
                    url,
                    json=data,
                    headers={'Authorization': f'Bearer {settings.MESSAGE_API_TOKEN}'},  # noqa: E501
                    timeout=aiohttp.ClientTimeout(total=60)) as resp:
                resp.raise_for_status()
                json_resp = await resp.json()
                if json_resp['code'] != 0:
                    raise Exception('failed')
                return True, json_resp.get('message', '')
        except aiohttp.ClientError:
            return False, ''

    new_status: MessageStatus
    if (not client or not msg_lst or
            msg_lst.date_end < datetime.now(timezone.utc)):
        new_status = MessageStatus.FAIL
    else:
        new_status = MessageStatus.SENDING
    # Если message_log==None, значит функция отправки сообщения вызывается из
    # обработки рассылки, соответственно создаем в БД объект сообщения
    if not message_log:
        message_log_dict = {
            'date_send': datetime.now(timezone.utc),
            'status': new_status}
        message_log_in = MessageLogCreatePut.model_validate(message_log_dict)
        # Т.к. send_msg выполняется одновременно для всех получателей, то и
        # одновременно происходит запись в БД. А в таком случае для каждой
        # такой async send_msg требуется своя async_session. Но передаваемые
        # send_msg объекты к этой своей async_session не принадлежат. Поэтому
        # для записи в БД будем передавать ключ, а не объект:
        message_log = await crud.message_log.create(
            async_session, message_log_in,
            msg_lst.id if msg_lst else None,
            client.id if client else None)
    # Иначе значит функция отправки сообщения вызывается при повторной отправке
    # сообщения из celery
    else:
        message_log.status = new_status.value
        async_session.add(message_log)
        await async_session.commit()
        await async_session.refresh(message_log)

    # Если статус отправки получился fail, то только записываем лог и выходим
    # проверка client и msg_list нужна только для static typechecker
    if new_status == MessageStatus.FAIL or not client or not msg_lst:
        return

    msg_lst_schema = MsgListInDB.model_validate(msg_lst, from_attributes=True)
    client_schema = ClientInDB.model_validate(client, from_attributes=True)
    if client_schema.can_receive_messages(msg_lst_schema.allowed_intervals):
        data = {
            "id": message_log.id,
            "phone": client.phone,
            "text": msg_lst.message_text
        }
        res, msg = await send_request(str(settings.MESSAGE_API_BASE) +
                                      f'send/{message_log.id}')
    else:
        res, msg = False, ''
    #  Если отправка была неудачной, то планируем повторную отправку в celery
    if not res and n_retries > 0:
        schedule_send_msg_task.apply_async((message_log.id, n_retries - 1),
                                           countdown=random.randint(60, 300))
        message_log.status = MessageStatus.SCHEDULED.value
    else:
        message_log.status = MessageStatus.SUCCESS.value if res \
            else MessageStatus.FAIL.value
        message_log.response_text = msg
    async_session.add(message_log)
    await async_session.commit()
    await async_session.refresh(message_log)


async def send_msg_with_new_session(
        msg_lst: MsgList,
        client: Client,
        http_session: aiohttp.ClientSession,
        n_retries: int = 5) -> None:
    async_session: AsyncSession | None = None
    try:
        async_session = SessionLocal()
        return await send_msg(
            msg_lst, client, http_session, async_session,
            n_retries)
    finally:
        if async_session:
            await async_session.close()


async def msg_list_process_async(obj_id: int) -> None:
    async_session: AsyncSession | None = None
    try:
        await engine.dispose(False)
        async_session = SessionLocal()
        stmt_msg_list = select(MsgList).where(
            MsgList.id == obj_id,
            MsgList.date_end >= datetime.now(timezone.utc))
        msg_list: MsgList | None = await async_session.scalar(stmt_msg_list)
        if not msg_list:
            return

        stmt_clients = select(Client)
        if msg_list.filter_props:
            tags = msg_list.filter_props.get('tags')
            mob_codes = msg_list.filter_props.get('mob_codes')
            if tags:
                stmt_clients = stmt_clients.where(Client.tag.in_(tags))
            if mob_codes:
                stmt_clients = stmt_clients.where(
                    Client.mob_code.in_(mob_codes))
        clients: Iterable[Client] = await async_session.scalars(stmt_clients)

        async with aiohttp.ClientSession() as http_session:
            tasks = [
                send_msg_with_new_session(msg_list, client, http_session)
                for client in clients]
            await asyncio.gather(*tasks)
    finally:
        if async_session:
            await async_session.close()


async def schedule_send_msg_main(message_log_id: int, n_retries: int) -> None:
    async_session: AsyncSession | None = None
    try:
        await engine.dispose(False)
        async_session = SessionLocal()
        stmt = select(MessageLog).where(
            MessageLog.id == message_log_id).options(
            selectinload(MessageLog.client), selectinload(MessageLog.msg_list))
        message_log: MessageLog | None = await async_session.scalar(stmt)
        if not message_log:
            raise Exception(f'MessageLog with ID={message_log_id} not found')
        async with aiohttp.ClientSession() as session:
            await send_msg(
                message_log.msg_list,
                message_log.client,
                session,
                async_session,
                n_retries,
                message_log)
    finally:
        if async_session:
            await async_session.close()


@shared_task
def msg_list_process_task(obj_id: int) -> None:
    # asyncio.get_event_loop().run_until_complete(msg_list_process_async(obj_id))
    asyncio.run(msg_list_process_async(obj_id))


@shared_task
def schedule_send_msg_task(message_log_id: int, n_retries: int) -> None:
    asyncio.run(schedule_send_msg_main(message_log_id, n_retries))
