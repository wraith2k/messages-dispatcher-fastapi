from datetime import datetime, time
from typing import Annotated, TypeVar

from pydantic import BaseModel, AwareDatetime, Field, AfterValidator, \
    model_validator, ConfigDict, PlainSerializer, PositiveInt

from app.schemas.common import MobCode, Tag

TimeOrDatetime = TypeVar("TimeOrDatetime", datetime, time)


def check_naive(v: TimeOrDatetime) -> TimeOrDatetime:
    assert v.tzinfo is None, 'Input should not have timezone info'
    return v


NaiveTime = Annotated[
    time,
    AfterValidator(check_naive),
    PlainSerializer(lambda v: v.isoformat(), return_type=str)
]


class FilterProps(BaseModel):
    mob_codes: Annotated[
                   set[MobCode],
                   Field(min_items=1, max_items=10),
                   PlainSerializer(lambda v: list(v), return_type=list)
               ] | None = None
    tags: Annotated[
              set[Tag],
              Field(min_items=1, max_items=10),
              PlainSerializer(lambda v: list(v), return_type=list)
          ] | None = None


# При записи в БД надо будет примерно так проверять модель
# mdl_to_write=mdl_in_db.model_copy(update=mdl_in_rest_right.model_dump(exclude_unset=True))
# MsgListBase.model_validate(mdl_to_write)
class MsgListBase(BaseModel):
    date_start: AwareDatetime | None = None
    date_end: AwareDatetime | None = None
    message_text: Annotated[
                      str, Field(min_length=1, max_length=200)] | None = None
    filter_props: FilterProps | None = None
    allowed_intervals: Annotated[
                           list[tuple[NaiveTime, NaiveTime]],
                           Field(min_items=1)
                       ] | None = None

    @model_validator(mode='after')
    def check_dates(self: 'MsgListBase') -> 'MsgListBase':
        if self.date_start and self.date_end:
            assert self.date_start <= self.date_end, \
                'date_start can not be later than date_end'
        return self


class MsgListInDB(MsgListBase):
    model_config = ConfigDict(from_attributes=True)

    id: PositiveInt


class MsgListCreatePut(MsgListBase):
    date_start: AwareDatetime
    date_end: AwareDatetime
    message_text: Annotated[str, Field(min_length=1, max_length=200)]


class MsgListPatch(MsgListBase):
    pass
