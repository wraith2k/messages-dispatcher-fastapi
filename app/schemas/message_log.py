from typing import Annotated
from pydantic import BaseModel, AwareDatetime, ConfigDict, PositiveInt, \
    Field, PlainSerializer

from app.schemas.common import MessageStatusEnum

MessageStatus = Annotated[
    MessageStatusEnum,
    Field(description=MessageStatusEnum.make_desc()),
    PlainSerializer(lambda v: v.value, return_type=str)]


class MessageLogBase(BaseModel):
    date_send: AwareDatetime | None = None
    status: MessageStatus | None = None
    response_text: str | None = None


class MessageLogInDB(MessageLogBase):
    model_config = ConfigDict(from_attributes=True)

    id: PositiveInt


class MessageLogCreatePut(MessageLogBase):
    date_send: AwareDatetime
    status: MessageStatus


class MessageLogPatch(MessageLogBase):
    pass
