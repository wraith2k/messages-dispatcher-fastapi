from typing import Annotated

from pydantic import BaseModel, AwareDatetime, ConfigDict, PositiveInt, \
    NonNegativeInt, Field

from app.schemas import MessageLogInDB, ClientInDB
from app.schemas.common import MessageStatusEnum


class MessageStatuses(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    sending: NonNegativeInt
    fail: NonNegativeInt
    success: NonNegativeInt
    scheduled: NonNegativeInt


class OverallReport(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    id: PositiveInt | None
    date_start: AwareDatetime | None
    date_end: AwareDatetime | None
    statuses: Annotated[
        MessageStatuses,
        Field(description=MessageStatusEnum.make_desc())]


class DetailReport(MessageLogInDB):
    model_config = ConfigDict(from_attributes=True)

    client: ClientInDB | None
