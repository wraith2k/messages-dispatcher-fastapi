from __future__ import annotations

from datetime import time, datetime, timezone
from zoneinfo import ZoneInfo

from pydantic import BaseModel, ConfigDict, PositiveInt
from app.schemas.common import MobCode, Tag, PhoneType, TimezoneType


class ClientBase(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)

    phone: PhoneType | None = None
    mob_code: MobCode | None = None
    tag: Tag | None = None
    time_zone: TimezoneType | None = None

    def can_receive_messages(
            self,
            intervals: list[tuple[time, time]] | None,
            check_time: datetime = datetime.now(timezone.utc)) -> bool:
        if intervals is None:
            return True
        client_time = check_time.astimezone(
            ZoneInfo(self.time_zone)
            if isinstance(self.time_zone, str)
            else self.time_zone
        ).time().replace(tzinfo=None)
        for tm_start, tm_end in intervals:
            if tm_start <= tm_end:
                if tm_start <= client_time <= tm_end:
                    return True
            else:
                if client_time >= tm_start or client_time <= tm_end:
                    return True
        return False


class ClientInDB(ClientBase):
    model_config = ConfigDict(from_attributes=True)

    id: PositiveInt


class ClientCreatePut(ClientBase):
    phone: PhoneType
    mob_code: MobCode
    time_zone: TimezoneType = 'UTC'


class ClientPatch(ClientBase):
    pass
