from typing import Annotated, TypeVar
from zoneinfo import ZoneInfo, ZoneInfoNotFoundError

from pydantic import Field, BeforeValidator, PlainSerializer, WithJsonSchema

from app.core.enum_with_desc import EnumWithDesc, ValueExtraInfo

MobCode = Annotated[str, Field(pattern=r'^\d{1,5}$')]
Tag = Annotated[str, Field(min_length=1, max_length=20)]
PhoneType = Annotated[str, Field(pattern=r'^7\d{10}$')]
T = TypeVar('T')


def make_zoneinfo(v: T) -> T | ZoneInfo:
    if not isinstance(v, str):
        return v
    try:
        tz = ZoneInfo(v)
    except ZoneInfoNotFoundError as e:
        raise ValueError(e)
    return tz


def check_zoneinfo(v: ZoneInfo) -> ZoneInfo:
    assert isinstance(v, ZoneInfo), 'Input must be str or ZoneInfo!'
    return v


TimezoneType = Annotated[
    ZoneInfo | str,
    BeforeValidator(check_zoneinfo),
    BeforeValidator(make_zoneinfo),
    PlainSerializer(lambda tz: tz.key, return_type=str),
    WithJsonSchema({
        'type': 'string',
        'format': 'timezone',
        'examples': ['Europe/Moscow', 'UTC', 'Etc/GMT+10']
    })
]


class MessageStatusEnum(EnumWithDesc):
    SENDING: Annotated[
        str,
        ValueExtraInfo('Message delivering to client')] = 'sending'
    FAIL: Annotated[
        str,
        ValueExtraInfo('Message delivery failed')] = 'fail'
    SUCCESS: Annotated[
        str,
        ValueExtraInfo('Message successfully delivered')] = 'success'
    SCHEDULED: Annotated[
        str,
        ValueExtraInfo('Message scheduled for retry sending')] = 'scheduled'
