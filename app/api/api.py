from fastapi import APIRouter
from app.api.endpoints import msg_lists, clients, reports

api_router = APIRouter()

api_router.include_router(reports.router,
                          prefix="/msg_lists",
                          tags=["Reports"])
api_router.include_router(msg_lists.router,
                          prefix="/msg_lists",
                          tags=["Message lists"])
api_router.include_router(clients.router,
                          prefix="/clients",
                          tags=["Clients"])
