from typing import Annotated, Sequence

from fastapi import APIRouter, Depends, HTTPException, Path, Query
from pydantic import PositiveInt, NonNegativeInt
from sqlalchemy.ext.asyncio import AsyncSession

from app import schemas
from app.api.deps import get_db
from app import crud

router = APIRouter()


@router.get("/", response_model=list[schemas.ClientInDB])
async def read_multi(
        skip: Annotated[NonNegativeInt, Query()] = 0,
        limit: Annotated[NonNegativeInt, Query()] = 100,
        async_session: AsyncSession = Depends(get_db)
) -> Sequence[crud.Client]:
    return await crud.client.get_multi(async_session, skip, limit)


@router.post("/", response_model=schemas.ClientInDB)
async def create_obj(
        *,
        async_session: AsyncSession = Depends(get_db),
        obj_in: schemas.ClientCreatePut,
) -> crud.Client:
    return await crud.client.create(async_session, obj_in)


@router.get("/{id}", response_model=schemas.ClientInDB)
async def read_obj(
        *,
        async_session: AsyncSession = Depends(get_db),
        id: Annotated[PositiveInt, Path()]
) -> crud.Client:
    obj = await crud.client.get(async_session, id)
    if not obj:
        raise HTTPException(status_code=404,
                            detail=f"Object with ID={id} not found")
    return obj


@router.put("/{id}", response_model=schemas.ClientInDB)
async def put_obj(
        *,
        async_session: AsyncSession = Depends(get_db),
        id: Annotated[PositiveInt, Path()],
        obj_in: schemas.ClientCreatePut
) -> crud.Client:
    obj = await crud.client.get(async_session, id)
    if not obj:
        raise HTTPException(status_code=404,
                            detail=f"Object with ID={id} not found")
    # Т.к. метод put, то перезаписывем все поля. Для этого на входе используем
    # ту же схему, что и для создания, далее создаем схему Patch в которой
    # будут заданы все поля, чтобы они при записи не пропустились из-за
    # аргумента exclude_unset.
    # Т.е. если в obj_in некоторые поля не заданы и поэтому имеют значение по
    # умолчанию, то в mdl_to_write уже все поля получатся заданы
    mdl_to_write = schemas.ClientPatch(**obj_in.model_dump())

    obj = await crud.client.update(async_session, obj, mdl_to_write)
    return obj


@router.patch("/{id}", response_model=schemas.ClientInDB)
async def patch_obj(
        *,
        async_session: AsyncSession = Depends(get_db),
        id: Annotated[PositiveInt, Path()],
        obj_in: schemas.ClientPatch
) -> crud.Client:
    obj = await crud.client.get(async_session, id)
    if not obj:
        raise HTTPException(status_code=404,
                            detail=f"Object with ID={id} not found")
    return await crud.client.update(async_session, obj, obj_in)


@router.delete("/{id}", response_model=schemas.ClientInDB)
async def delete_obj(
        *,
        async_session: AsyncSession = Depends(get_db),
        id: Annotated[PositiveInt, Path()]
) -> crud.Client:
    obj = await crud.client.remove(async_session, id)
    if not obj:
        raise HTTPException(status_code=404,
                            detail=f"Object with ID={id} not found")
    return obj
