from typing import Annotated, Sequence

from fastapi import APIRouter, Depends, Path
from pydantic import PositiveInt
from sqlalchemy.ext.asyncio import AsyncSession

from app import schemas
from app.api.deps import get_db
from app.models.message_log import MessageLog
from app.reports.detail_report import detail_report
from app.reports.overall_report import overall_report

router = APIRouter()


@router.get("/stat", response_model=list[schemas.OverallReport])
async def all_stat(
        *,
        async_session: AsyncSession = Depends(get_db)
) -> Sequence:
    return await overall_report(async_session)


@router.get("/{id}/stat", response_model=list[schemas.DetailReport])
async def detail_stat(
        *,
        async_session: AsyncSession = Depends(get_db),
        id: Annotated[PositiveInt, Path()]
) -> Sequence[MessageLog]:
    return await detail_report(async_session, id)
