from typing import AsyncGenerator

from sqlalchemy.ext.asyncio import AsyncSession

from app.db.session import SessionLocal


async def get_db() -> AsyncGenerator:
    async_session: AsyncSession | None = None
    try:
        async_session = SessionLocal()
        yield async_session
    finally:
        if async_session:
            await async_session.close()
