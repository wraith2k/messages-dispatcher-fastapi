"""initial

Revision ID: 87c41c03249d
Revises: 
Create Date: 2023-09-10 00:38:47.616468

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision: str = '87c41c03249d'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('messages_dispatcher_client',
    sa.Column('phone', sa.String(length=11), nullable=False),
    sa.Column('mob_code', sa.String(length=5), nullable=False),
    sa.Column('tag', sa.String(length=20), nullable=True),
    sa.Column('time_zone', sa.String(length=50), nullable=False),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_messages_dispatcher_client_id'), 'messages_dispatcher_client', ['id'], unique=False)
    op.create_table('messages_dispatcher_msglist',
    sa.Column('date_start', postgresql.TIMESTAMP(timezone=True), nullable=False),
    sa.Column('date_end', postgresql.TIMESTAMP(timezone=True), nullable=False),
    sa.Column('message_text', sa.String(length=200), nullable=False),
    sa.Column('filter_props', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
    sa.Column('allowed_intervals', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_messages_dispatcher_msglist_id'), 'messages_dispatcher_msglist', ['id'], unique=False)
    op.create_table('messages_dispatcher_messagelog',
    sa.Column('date_send', postgresql.TIMESTAMP(timezone=True), nullable=False),
    sa.Column('status', sa.String(length=20), nullable=False),
    sa.Column('response_text', sa.String(length=200), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('msg_list_id', sa.Integer(), nullable=True),
    sa.Column('client_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['client_id'], ['messages_dispatcher_client.id'], ),
    sa.ForeignKeyConstraint(['msg_list_id'], ['messages_dispatcher_msglist.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_messages_dispatcher_messagelog_id'), 'messages_dispatcher_messagelog', ['id'], unique=False)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_messages_dispatcher_messagelog_id'), table_name='messages_dispatcher_messagelog')
    op.drop_table('messages_dispatcher_messagelog')
    op.drop_index(op.f('ix_messages_dispatcher_msglist_id'), table_name='messages_dispatcher_msglist')
    op.drop_table('messages_dispatcher_msglist')
    op.drop_index(op.f('ix_messages_dispatcher_client_id'), table_name='messages_dispatcher_client')
    op.drop_table('messages_dispatcher_client')
    # ### end Alembic commands ###
